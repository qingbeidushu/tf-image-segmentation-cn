<div class="Box-sc-g0xbh4-0 bJMeLZ js-snippet-clipboard-copy-unpositioned" data-hpc="true"><article class="markdown-body entry-content container-lg" itemprop="text"><div class="markdown-heading" dir="auto"><h1 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">TF Image Segmentation：图像分割框架</font></font></h1><a id="user-content-tf-image-segmentation-image-segmentation-framework" class="anchor" aria-label="永久链接：TF 图像分割：图像分割框架" href="#tf-image-segmentation-image-segmentation-framework"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">该框架的目的</font></font><code>TF Image Segmentation</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">是提供/提供一种简化的方法：</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">将一些流行的通用/医学/其他图像分割数据集转换为易于使用的</font></font><code>.tfrecords</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
具有统一接口的训练格式：不同的数据集，但存储图像和注释的方式相同。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">具有动态数据增强（缩放、颜色失真）的训练例程。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">经证明适用于特定模型/数据集对的训练例程。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用常见的准确度测量来评估训练模型的准确度：平均 IOU、平均像素。</font><font style="vertical-align: inherit;">精度，像素精度。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在特定数据集上训练并报告准确度的模型文件（使用 TF 和报告的训练例程训练的模型，而不是从 Caffe 或其他框架转换的模型）</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">模型定义（如 FCN-32 等）使用由 TF-Slim 库官方提供的图像分类模型（如 VGG）的权重初始化。</font></font></li>
</ul>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">到目前为止，该框架包含 Tensorflow 和 TF-Slim 库中 FCN 模型（训练和评估）的实现，其中包含训练例程、报告的准确性以及 PASCAL VOC 2012 数据集的训练模型。</font><font style="vertical-align: inherit;">要根据您的数据训练这些模型，</font></font><a href="/warmspringwinds/tf-image-segmentation/blob/master/tf_image_segmentation/recipes/pascal_voc/convert_pascal_voc_to_tfrecords.ipynb"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">请将数据集转换为 tfrecords</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">并按照以下说明进行操作。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">最终目标是提供实用程序来转换其他数据集、报告其准确性并提供模型。</font></font></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">安装</font></font></h2><a id="user-content-installation" class="anchor" aria-label="永久链接：安装" href="#installation"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">这段代码需要：</font></font></p>
<ol dir="auto">
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Tensorflow</font></font><code>r0.12</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">或更高版本。</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">自定义</font></font><a href="https://github.com/tensorflow/models"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">张量流/模型</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">存储库，将来</font><font style="vertical-align: inherit;">可能会</font></font><a href="https://github.com/tensorflow/models/pull/684" data-hovercard-type="pull_request" data-hovercard-url="/tensorflow/models/pull/684/hovercard"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">合并。</font></font></a><font style="vertical-align: inherit;"></font></p>
</li>
</ol>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">只需运行：</font></font></p>
<p dir="auto"><code>git clone -b fully_conv_vgg https://github.com/warmspringwinds/models</code></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">并将</font></font><code>models/slim</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">子目录添加到您的路径中：</font></font></p>
<div class="highlight highlight-source-python notranslate position-relative overflow-auto" dir="auto"><pre><span class="pl-k">import</span> <span class="pl-s1">sys</span>
<span class="pl-c"># update with your path</span>
<span class="pl-s1">sys</span>.<span class="pl-s1">path</span>.<span class="pl-en">append</span>(<span class="pl-s">'/home/dpakhom1/workspace/models/slim/'</span>)</pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="import sys
# update with your path
sys.path.append('/home/dpakhom1/workspace/models/slim/')" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<ol start="3" dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">一些库可以通过安装</font></font><a href="https://www.continuum.io/downloads" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Anaconda 包</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">来获取。</font></font></li>
</ol>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">或者您可以</font><font style="vertical-align: inherit;">使用 来安装</font></font><code>scikit-image</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">, </font></font><code>matplotlib</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">, </font><font style="vertical-align: inherit;">。</font></font><code>numpy</code><font style="vertical-align: inherit;"></font><code>pip</code><font style="vertical-align: inherit;"></font></p>
<ol start="4" dir="auto">
<li>
<p dir="auto"><code>VGG 16</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">检查点文件，您可以从</font></font><a href="http://download.tensorflow.org/models/vgg_16_2016_08_28.tar.gz" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">此处</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">获取该文件。</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">克隆这个库：</font></font></p>
</li>
</ol>
<p dir="auto"><code>git clone https://github.com/warmspringwinds/tf-image-segmentation</code></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">并将其添加到路径中：</font></font></p>
<div class="highlight highlight-source-python notranslate position-relative overflow-auto" dir="auto"><pre><span class="pl-k">import</span> <span class="pl-s1">sys</span>
<span class="pl-c"># update with your path</span>
<span class="pl-s1">sys</span>.<span class="pl-s1">path</span>.<span class="pl-en">append</span>(<span class="pl-s">"/home/dpakhom1/tf_projects/segmentation/tf-image-segmentation/"</span>)</pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="import sys
# update with your path
sys.path.append(&quot;/home/dpakhom1/tf_projects/segmentation/tf-image-segmentation/&quot;)" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">帕斯卡VOC 2012</font></font></h2><a id="user-content-pascal-voc-2012" class="anchor" aria-label="永久链接：帕斯卡 VOC 2012" href="#pascal-voc-2012"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">已实现的模型在受限 PASCAL VOC 2012 验证数据集 (RV-VOC12) 上进行了测试，并在 PASCAL VOC 2012 训练数据和 PASCAL VOC 12 的附加 Berkeley 分割数据上进行了训练。在受限验证数据集上测试模型以确保没有图像非常重要在训练期间模型可以看到验证数据集中的内容。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">框架中还提供了获取训练和验证模型的代码。</font></font></p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">用于语义分割（FCN）的全卷积网络</font></font></h3><a id="user-content-fully-convolutional-networks-for-semantic-segmentation-fcns" class="anchor" aria-label="永久链接：用于语义分割（FCN）的全卷积网络" href="#fully-convolutional-networks-for-semantic-segmentation-fcns"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在这里您可以找到 Long 等人的论文“用于语义分割的完全卷积网络”中描述的模型。</font><font style="vertical-align: inherit;">我们针对 PASCAL VOC 2012 数据集进行了训练</font></font><code>FCN-32s</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">和</font><font style="vertical-align: inherit;">测试</font></font><code>FCN-16s</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。</font></font><code>FCN-8s</code><font style="vertical-align: inherit;"></font></p>
<p dir="auto"><font style="vertical-align: inherit;"></font><a href="/warmspringwinds/tf-image-segmentation/blob/master/tf_image_segmentation/recipes/pascal_voc/FCNs"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">您可以在此处</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">找到用于培训和评估的所有脚本</font><font style="vertical-align: inherit;">。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">该代码已用于训练具有以下性能的网络：</font></font></p>
<table>
<thead>
<tr>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">模型</font></font></th>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">测试数据</font></font></th>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">平均借据</font></font></th>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">平均像素。</font><font style="vertical-align: inherit;">准确性</font></font></th>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">像素精度</font></font></th>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">模型下载链接</font></font></th>
</tr>
</thead>
<tbody>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">FCN-32（我们的）</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">RV-VOC12</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">62.70</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在程序中。</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在程序中。</font></font></td>
<td><a href="https://www.dropbox.com/s/66coqapbva7jpnt/fcn_32s.tar.gz?dl=0" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Dropbox</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">FCN-16（我们的）</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">RV-VOC12</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">63.52</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在程序中。</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在程序中。</font></font></td>
<td><a href="https://www.dropbox.com/s/tmhblqcwqvt2zjo/fcn_16s.tar.gz?dl=0" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Dropbox</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">FCN-8（我们的）</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">RV-VOC12</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">63.65</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在程序中。</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在程序中。</font></font></td>
<td><a href="https://www.dropbox.com/s/7r6lnilgt78ljia/fcn_8s.tar.gz?dl=0" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Dropbox</font></font></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">FCN-32s（原版）</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">RV-VOC11</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">59.40</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">73.30</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">89.10</font></font></td>
<td></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">FCN-16s（原版）</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">RV-VOC11</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">62.40</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">75.70</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">90.00</font></font></td>
<td></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">FCN-8s（原版）</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">RV-VOC11</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">62.70</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">75.90</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">90.30</font></font></td>
<td></td>
</tr>
</tbody>
</table>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">关于</font></font></h2><a id="user-content-about" class="anchor" aria-label="永久链接：关于" href="#about"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如果您使用该代码进行研究，请引用该论文：</font></font></p>
<div class="snippet-clipboard-content notranslate position-relative overflow-auto"><pre class="notranslate"><code>@article{pakhomov2017deep,
  title={Deep Residual Learning for Instrument Segmentation in Robotic Surgery},
  author={Pakhomov, Daniil and Premachandran, Vittal and Allan, Max and Azizian, Mahdi and Navab, Nassir},
  journal={arXiv preprint arXiv:1703.08580},
  year={2017}
}
</code></pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="@article{pakhomov2017deep,
  title={Deep Residual Learning for Instrument Segmentation in Robotic Surgery},
  author={Pakhomov, Daniil and Premachandran, Vittal and Allan, Max and Azizian, Mahdi and Navab, Nassir},
  journal={arXiv preprint arXiv:1703.08580},
  year={2017}
}" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在实施过程中，报告了一些初步实验和注意事项：</font></font></p>
<ul dir="auto">
<li><a href="http://warmspringwinds.github.io/tensorflow/tf-slim/2016/10/30/image-classification-and-segmentation-using-tensorflow-and-tf-slim/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">将图像分类网络转换为 FCN</font></font></a></li>
<li><a href="http://warmspringwinds.github.io/tensorflow/tf-slim/2016/11/22/upsampling-and-image-segmentation-with-tensorflow-and-tf-slim/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用转置卷积执行上采样</font></font></a></li>
<li><a href="http://warmspringwinds.github.io/tensorflow/tf-slim/2016/12/18/image-segmentation-with-tensorflow-using-cnns-and-conditional-random-fields/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">用于细化 FCN-32s 模型分割的分割和粗略度的条件随机场</font></font></a></li>
<li><a href="http://warmspringwinds.github.io/tensorflow/tf-slim/2016/12/21/tfrecords-guide/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">TF 记录使用情况</font></font></a></li>
</ul>
</article></div>
